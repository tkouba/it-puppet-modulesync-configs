## 2016-12-07
+ Add simplecov-console, needed by selinux module.
+ Add --full-index to bundle install, # of gems related.

## 2016-12-02
+ Add net-ldap to list of gems for DB.

## 2016-11-23
+ Pin beaker to work with ruby 2.
+ Install coveralls gem.
+ Install puppet-lint-variable_contains_upcase

## 2016-10-19
+ yaml-lint tests no longer fail if data directory is simply not 
  present.
+ Docker file now has less layers so is smaller.
+ The .sync.yml file is now syntax tested, it being
  bad on one module could cause all modules to fail
  to sync.
+ Uses new Docker image by default, the mater branch of the 
  ci_images module. The image to use can be customised via
  modulesync.

## 2016-09-27
+ New variable can be set in .sync.yml

```yaml
.gitlab-ci.yml:
  all_allow_failure: true
```
It sets all rake tests to be permitted to fail, useful for when
you want to enaable tests but not enforce them.

## 2016-09-26
+ Generate and use customtised docker image.

## 2016-07-05
+ metadata_lint check failing if LANG=en_US.UTF-8 not set.

## 2016-06-23
+ Updated gem list to work with puppet-lint 2.0.0
+ For now disable puppet-lint-variable_contains_upcase pending
  https://github.com/fiddyspence/puppetlint-variablecase/pull/2

## 2016-03-07
* Add more links to README.md.
* Add a CHANGELOG to the sync configurations.
