<table>
<tr>
  <th>Module or Hostgroup</th>
  <th>Master Branch</th>
  <th>QA Branch</th>
  <th>Modulesync Branch</th>
  <th>Coverage QA Branch</th>
</tr>
<tr>
 <td><a href='https://gitlab.cern.ch/ai/<%= @configs[:puppet_module] %>'><%= @configs[:puppet_module] %></a></td>
 <% ['master','qa','modulesync'].each do | b | -%>
 <td><a href='https://gitlab.cern.ch/ai/<%= @configs[:puppet_module] %>/commits/<%= b %>'>
     <img src='https://gitlab.cern.ch/ai/<%= @configs[:puppet_module] %>/badges/<%= b %>/build.svg'></a></td>
 <% end -%>
  <td>
  <a href="https://gitlab.cern.ch/ai/<%= @configs[:puppet_module] %>/commits/qa">
  <img alt="coverage report" src="https://gitlab.cern.ch/ai/<%= @configs[:puppet_module] %>/badges/qa/coverage.svg" /></a>
  </td>
</tr>
</table>

Table of all modules/hostgroups: [BUILDRESULTS.md](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs/blob/master/BUILDRESULTS.md)

* [code/ -  module directory](code/)
* [code/README.md - module documentation](code/README.md)
* [code/metadata.json - module metadata](code/metadata.json)
* [code/manifests - module manifests directory](code/manifests)
* [code/templates - module templates directory](code/templates)
* [code/files - module files directory](code/files)
* [code/lib module libs directory](code/lib)
* [data/ - module yaml data directory](data/)
* [ci/ - centrally maintained files for continuous integration directory](ci/)

* [CERN Configuration Guide](https://configdocs.web.cern.ch/configdocs/)
* [CERN modulesync configuration](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs)
* [CERN modulesync changelog](https://gitlab.cern.ch/ai/it-puppet-modulesync-configs/blob/master/README.md)

<% @configs['urls'].each do |url| -%>
* <%= url %>
<% end -%>


This file is maintained with modulesync.



