# CERN Puppet Module Sync Configuration

This repository contains templated files that can be synced
to all puppet modules and hostgroups. The
[modulesync](https://github.com/voxpupuli/modulesync) utility is
used for this.

To enable your puppet module to be updated please 
add it the file [managed_modules.yml](managed_modules.yml) with a merge request.

Once a module is enabled a new branch called *modulesync*
will be created by an overnight rundeck job.
This will be a branch from *qa* with synced files applied
on top as a commit. If no changes need to be applied because *qa* is
already in sync with these templates no branch will be created.

Once you merge the *modulesync* branch into *qa* you can safely
delete the *modulesync* branch.


# Which files are and are not synced.
The full collection of files to be synced are locationed
in [moduleroot](moduleroot). In particular the following files are
maintained.

* `README.md` top level README.md file.
* `.gitlab-ci.yml` contains test command matrix.
* `.gitignore` ignore some files, especially ones created during testing.
* `ci/Rakefile` rake tasks to do testing.
* `ci/Gemfiles` lists ruby gems to do testing.

The files inside `code` and `data` will never be touched by this modulesync.

## Test Information
Tests that are run

* puppet lint tests.  Rake target *lint*.
* puppet validate tests. Rake target *validate*.
* puppet rspec tests. Rake target *spec*.

Acceptance tests are not currently executed - WIP.

## Running Tests Locally
Follow the instructions in `.gitlab-ci.yml` for exact commands of tests though
you may need to drop the `--local` flag if you have not downloaded
the gems once.

One example that works on *aiadm7.cern.ch*

```bash
git clone https://:@gitlab.cern.ch:8443/ai/it-puppet-module-yourmodule.git
cd code
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 4.8.0' bundle install --without system_tests development
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 4.8.0' bundle exec rake --rakefile ../ci/Rakefile test
```

You can switch from Puppet3 to Puppet4 or particular version of Puppet4 by issuing 
bundle update, there's no need to install anything from scratch. 
For instance, to jump to Puppet3:

```bash
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 3.0' bundle update
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 3.0' bundle exec rake --rakefile ../ci/Rakefile test
```

To jump to the latest puppet 4.8.X version:

```bash
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 4.8.0' bundle update
BUNDLE_GEMFILE=../ci/Gemfile PUPPET_VERSION='~> 4.8.0' bundle exec rake --rakefile ../ci/Rakefile test
```


In time the commands above will simplified.

## Running the tests on GitLab
Pipelines running your tests will only start if GitLab CI is enabled on the 
repository where you're running your tests. 
[The GitLab documentation](https://docs.gitlab.com/ce/ci/enable_or_disable_ci.html) 
describes  what flag has to be activated to enable it.

## Merge Request Configuration
It is recomended to configure your module's merge request to use
`Merge commit with semi-linear history` or `Fast-forward merge`.
[The GitLab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html)
Without this a merge request passing tests may result in failed tests after merge.

## Adjusting or Disabling Particular Tests
The *modulesync* utility has the ability to influenced by
a control file within your own repository. Create a file `.sync.yml`
in your repository at the top level.

Below there is an example that will:

* Disable the rake target that runs spec tests.
* Disable a particular lint test.
* Exclude an extra directory from being processed for lint
  or compile validation.
* Configure `strict_variable` environment to rspec puppet.

```yaml
---
ci/Rakefile:
  extra_disabled_rake_targets:
     - spec
  extra_disabled_lint_checks:
     - 'disable_quoted_booleans'
  extra_exclude_paths:
     - examples/**/*
.gitlab-ci.yml:
  strict_variables: 'no'
```

Once a .sync.yml file is present the next run of modulesync will create
the modulesync branch with these updates which can then be merged in the normal
way.

For full details of what can be set 
read the [source](moduleroot) and [default configuration](config_defaults.yml).


## Running modulesync yourself.

The *msync* command is available on aiadm.

```bash
git clone https://:@gitlab.cern.ch:8443/ai/it-puppet-modulesync-configs.git
cd it-puppet-modulesync-configs
msync update --noop -f it-puppet-module-<yourmodle>
msync update -f it-puppet-module-<yourmodle>
```




