# Current puppet module build results



<table>
<tr>
  <th>Module or Hostgroup</th>
  <th>Master Brach</th>
  <th>QA Branch</th>
  <th>Modulesync Branch</th>
</tr>
<tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc'>it-puppet-hostgroup-acronsvc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-acronsvc/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm'>it-puppet-hostgroup-aiadm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-aiadm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver'>it-puppet-hostgroup-appserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-appserver/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup'>it-puppet-hostgroup-backup</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-backup/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi'>it-puppet-hostgroup-bi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-bi/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor'>it-puppet-hostgroup-castor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog'>it-puppet-hostgroup-castortapelog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-castortapelog/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph'>it-puppet-hostgroup-ceph</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ceph/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm'>it-puppet-hostgroup-cloud_adm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_adm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation'>it-puppet-hostgroup-cloud_automation</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_automation/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal'>it-puppet-hostgroup-cloud_baremetal</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_baremetal/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage'>it-puppet-hostgroup-cloud_blockstorage</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_blockstorage/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard'>it-puppet-hostgroup-cloud_dashboard</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_dashboard/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity'>it-puppet-hostgroup-cloud_identity</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_identity/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image'>it-puppet-hostgroup-cloud_image</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_image/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring'>it-puppet-hostgroup-cloud_monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_monitoring/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement'>it-puppet-hostgroup-cloud_projectmanagement</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_projectmanagement/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry'>it-puppet-hostgroup-cloud_telemetry</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_telemetry/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow'>it-puppet-hostgroup-cloud_workflow</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cloud_workflow/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview'>it-puppet-hostgroup-codereview</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-codereview/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor'>it-puppet-hostgroup-condor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-condor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta'>it-puppet-hostgroup-cta</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cta/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs'>it-puppet-hostgroup-cvmfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-cvmfs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database'>it-puppet-hostgroup-database</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-database/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod'>it-puppet-hostgroup-dbod</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-dbod/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos'>it-puppet-hostgroup-eos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-eos/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer'>it-puppet-hostgroup-filer</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-filer/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge'>it-puppet-hostgroup-forge</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-forge/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git'>it-puppet-hostgroup-git</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-git/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire'>it-puppet-hostgroup-inspire</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-inspire/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es'>it-puppet-hostgroup-it_es</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-it_es/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its'>it-puppet-hostgroup-its</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-its/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon'>it-puppet-hostgroup-licmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-licmon/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth'>it-puppet-hostgroup-lsfauth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lsfauth/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus'>it-puppet-hostgroup-lxplus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-lxplus/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir'>it-puppet-hostgroup-mimir</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-mimir/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring'>it-puppet-hostgroup-monitoring</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-monitoring/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth'>it-puppet-hostgroup-ngauth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-ngauth/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile'>it-puppet-hostgroup-nile</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-nile/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas'>it-puppet-hostgroup-paas</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch'>it-puppet-hostgroup-punch</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-punch/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security'>it-puppet-hostgroup-security</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-security/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn'>it-puppet-hostgroup-svn</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-svn/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust'>it-puppet-hostgroup-tapedust</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapedust/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver'>it-puppet-hostgroup-tapeserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-tapeserver/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon'>it-puppet-hostgroup-vcsmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-vcsmon/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami'>it-puppet-hostgroup-voatlasami</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasami/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics'>it-puppet-hostgroup-voatlasanalytics</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasanalytics/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild'>it-puppet-hostgroup-voatlasbuild</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasbuild/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm'>it-puppet-hostgroup-voatlasddm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasddm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc'>it-puppet-hostgroup-voatlasgcc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasgcc/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc'>it-puppet-hostgroup-voatlasmisc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasmisc/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio'>it-puppet-hostgroup-voatlasrucio</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-voatlasrucio/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud'>it-puppet-hostgroup-wlcghammercloud</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-hostgroup-wlcghammercloud/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs'>it-puppet-module-afs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-afs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-afs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools'>it-puppet-module-aitools</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-aitools/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-aitools/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato'>it-puppet-module-apiato</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-apiato/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-apiato/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive'>it-puppet-module-archive</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-archive/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-archive/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks'>it-puppet-module-auks</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-auks/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-auks/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper'>it-puppet-module-authwrapper</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-authwrapper/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats'>it-puppet-module-awstats</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-awstats/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-awstats/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus'>it-puppet-module-bagplus</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bagplus/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base'>it-puppet-module-base</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-base/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-base/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii'>it-puppet-module-bdii</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bdii/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bdii/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats'>it-puppet-module-beats</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-beats/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-beats/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro'>it-puppet-module-bro</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-bro/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-bro/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor'>it-puppet-module-cadvisor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cadvisor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor'>it-puppet-module-castor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-castor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-castor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph'>it-puppet-module-ceph</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ceph/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ceph/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients'>it-puppet-module-cern_physics_storage_clients</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cern_physics_storage_clients/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd'>it-puppet-module-cerncollectd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncollectd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor'>it-puppet-module-cerncondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerncondor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw'>it-puppet-module-cernfw</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernfw/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib'>it-puppet-module-cernlib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernlib/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco'>it-puppet-module-cernmco</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernmco/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet'>it-puppet-module-cernpuppet</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernpuppet/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck'>it-puppet-module-cernrundeck</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cernrundeck/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime'>it-puppet-module-cerntime</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cerntime/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr'>it-puppet-module-certmgr</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-certmgr/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images'>it-puppet-module-ci_images</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ci_images/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common'>it-puppet-module-cloud_common</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cloud_common/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd'>it-puppet-module-collectd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor'>it-puppet-module-collectd_cadvisor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-collectd_cadvisor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda'>it-puppet-module-conda</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-conda/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-conda/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta'>it-puppet-module-cta</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cta/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cta/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom'>it-puppet-module-custom</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-custom/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-custom/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs'>it-puppet-module-cvmfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-cvmfs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam'>it-puppet-module-dam</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dam/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dam/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig'>it-puppet-module-dbcoreconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbcoreconfig/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig'>it-puppet-module-dbhwbunchconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbhwbunchconfig/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon'>it-puppet-module-dbmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbmon/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig'>it-puppet-module-dbnetworkconfig</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbnetworkconfig/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod'>it-puppet-module-dbod</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dbod/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dbod/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite'>it-puppet-module-dmlite</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dmlite/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker'>it-puppet-module-docker</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-docker/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-docker/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm'>it-puppet-module-dpm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dpm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dpm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc'>it-puppet-module-dsc</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-dsc/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-dsc/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos'>it-puppet-module-eos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eos/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient'>it-puppet-module-eosclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-eosclient/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl'>it-puppet-module-fetchcrl</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fetchcrl/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume'>it-puppet-module-flume</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-flume/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-flume/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts'>it-puppet-module-fts</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-fts/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-fts/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2'>it-puppet-module-gfal2</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gfal2/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab'>it-puppet-module-gitlab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitlab/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3'>it-puppet-module-gitolite3</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitolite3/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitweb'>it-puppet-module-gitweb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gitweb/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn'>it-puppet-module-glexecwn</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-glexecwn/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite'>it-puppet-module-graphite</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-graphite/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-graphite/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp'>it-puppet-module-gridftp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-gridftp/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware'>it-puppet-module-hardware</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardware/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardware/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams'>it-puppet-module-hardwareparams</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hardwareparams/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hdp'>it-puppet-module-hdp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hdp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hdp/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hdp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hdp/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hdp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hdp/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess'>it-puppet-module-hraccess</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-hraccess/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor'>it-puppet-module-htcondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce'>it-puppet-module-htcondor_ce</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-htcondor_ce/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio'>it-puppet-module-invenio</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-invenio/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-invenio/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks'>it-puppet-module-java_ks</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-java_ks/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens'>it-puppet-module-jens</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jens/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jens/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira'>it-puppet-module-jira</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-jira/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-jira/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka'>it-puppet-module-kafka</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kafka/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kafka/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos'>it-puppet-module-kerberos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kerberos/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab'>it-puppet-module-keytab</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-keytab/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-keytab/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4'>it-puppet-module-kibana4</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kibana4/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes'>it-puppet-module-kubernetes</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-kubernetes/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb'>it-puppet-module-landb</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-landb/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-landb/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient'>it-puppet-module-lbclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lbclient/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm'>it-puppet-module-lcgdm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lcgdm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon'>it-puppet-module-lemon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemon/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd'>it-puppet-module-lemonfwd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lemonfwd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon'>it-puppet-module-licmon</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-licmon/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-licmon/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits'>it-puppet-module-limits</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-limits/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-limits/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate'>it-puppet-module-logrotate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-logrotate/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf'>it-puppet-module-lsf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-lsf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-lsf/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp'>it-puppet-module-misp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-misp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-misp/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi'>it-puppet-module-mpi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mpi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mpi/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor'>it-puppet-module-multifactor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-multifactor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql'>it-puppet-module-mysql</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-mysql/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-mysql/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios'>it-puppet-module-nagios</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nagios/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nagios/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs'>it-puppet-module-nfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nfs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth'>it-puppet-module-ngauth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ngauth/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile'>it-puppet-module-nile</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nile/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nile/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd'>it-puppet-module-nscd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-nscd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-nscd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp'>it-puppet-module-ntp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ntp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ntp/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent'>it-puppet-module-ocsagent</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ocsagent/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ofed'>it-puppet-module-ofed</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ofed/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ofed/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ofed/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ofed/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ofed/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ofed/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill'>it-puppet-module-oom_kill</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oom_kill/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie'>it-puppet-module-oozie</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oozie/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oozie/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients'>it-puppet-module-openstack_clients</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-openstack_clients/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12'>it-puppet-module-oracle_rdbms_12</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-oracle_rdbms_12/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos'>it-puppet-module-osrepos</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-osrepos/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker'>it-puppet-module-pacemaker</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-pacemaker/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch'>it-puppet-module-patch</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-patch/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-patch/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest'>it-puppet-module-postgrest</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-postgrest/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct'>it-puppet-module-psacct</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-psacct/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-psacct/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver'>it-puppet-module-puppetserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-puppetserver/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot'>it-puppet-module-reboot</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-reboot/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-reboot/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis'>it-puppet-module-redis</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-redis/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-redis/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog'>it-puppet-module-rsyslog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rsyslog/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck'>it-puppet-module-rundeck</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-rundeck/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security'>it-puppet-module-security</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-security/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-security/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux'>it-puppet-module-selinux</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-selinux/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-selinux/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail'>it-puppet-module-sendmail</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sendmail/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf'>it-puppet-module-serf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-serf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-serf/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth'>it-puppet-module-shibboleth</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-shibboleth/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm'>it-puppet-module-slurm</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-slurm/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-slurm/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp'>it-puppet-module-snmp</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-snmp/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-snmp/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk'>it-puppet-module-splunk</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunk/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunk/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder'>it-puppet-module-splunkforwarder</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-splunkforwarder/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid'>it-puppet-module-squid</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-squid/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-squid/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh'>it-puppet-module-ssh</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-ssh/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-ssh/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate'>it-puppet-module-sslcertificate</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sslcertificate/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd'>it-puppet-module-sssd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sssd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sssd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib'>it-puppet-module-stdlib</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-stdlib/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts'>it-puppet-module-sts</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sts/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sts/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo'>it-puppet-module-sudo</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sudo/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sudo/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins'>it-puppet-module-sysadmins</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysadmins/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs'>it-puppet-module-sysfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-sysfs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi'>it-puppet-module-teigi</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-teigi/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-teigi/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf'>it-puppet-module-telegraf</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-telegraf/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat'>it-puppet-module-tomcat</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tomcat/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient'>it-puppet-module-tsmclient</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmclient/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver'>it-puppet-module-tsmserver</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-tsmserver/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd'>it-puppet-module-umd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-umd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-umd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas'>it-puppet-module-vegas</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vegas/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vegas/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools'>it-puppet-module-voatlastools</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-voatlastools/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor'>it-puppet-module-vocmshtcondor</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport'>it-puppet-module-vosupport</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-vosupport/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog'>it-puppet-module-watchdog</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-watchdog/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn'>it-puppet-module-websvn</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-websvn/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-websvn/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6'>it-puppet-module-windows_disable_ipv6</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windows_disable_ipv6/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature'>it-puppet-module-windowsfeature</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-windowsfeature/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client'>it-puppet-module-wsus_client</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-wsus_client/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd'>it-puppet-module-xinetd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xinetd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd'>it-puppet-module-xrootd</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-xrootd/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum'>it-puppet-module-yum</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-yum/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-yum/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs'>it-puppet-module-zfs</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zfs/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zfs/badges/modulesync/build.svg'></a></td>
 <tr>
 <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper'>it-puppet-module-zookeeper</a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/master'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/master/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/qa'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/qa/build.svg'></a></td>
  <td><a href='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/commits/modulesync'>
     <img src='https://gitlab.cern.ch/ai/it-puppet-module-zookeeper/badges/modulesync/build.svg'></a></td>
 </table>


